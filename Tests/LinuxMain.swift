import XCTest

import GitLabListenerTests

var tests = [XCTestCaseEntry]()
tests += GitLabListenerTests.allTests()
XCTMain(tests)
