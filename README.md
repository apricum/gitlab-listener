# Documentation

GitLab Listener is a self-sufficient application that acts as a receiver of GitLab events and can be set up to react to different notifications with configurable commands. Behind the facade, it is a HTTP server process set up to run as a daemon, loads a user configuration on launch and then listens for requests on predefined endpoints. Through the use of GitLab webhooks, events are sent to the listener automatically whenever changes are made to a repository — like commit pushes, tags or merge request actions — which can then trigger a custom action in return.

## Build

Listener is written in Swift, an open source language originally sketched out and developed by Apple. It uses the [Perfect framework](https://perfect.org) for its HTTP server component. At the time of writing, Swift programs can be compiled to macOS and Linux on various architectures. It’s outside the scope of this guide for the moment to describe all compilation steps, hence only a summary is provided. An official “Getting Started” guide is provided on [swift.org/getting-started](https://swift.org/getting-started). For Linux, a prebuilt binary is already provided for important versions, see the “Releases” section.

On Mac, any system with Xcode command line tools installed should be able to install dependencies by running `carthage build` in the project’s root directory and then compiling by executing the general build command `swift build -c release`. The project can also be opened in Xcode and built from there.

On Linux, Swift needs to be installed manually or, if the used distro has the needed package sources available, installed through the local package manager. Alternatively, the public Docker image `swift:latest` can be used. Using Docker on macOS also allows to build a distributable for Linux without requiring an actual Linux system ahead of time.

## Function

Once running, Listener serves a primary and a secondary type of endpoint for processing and reacting to requests: (1) GitLab webhooks sent as `POST` to the route `/event`, as well as custom actions that are easy to call from the outside, sent as `GET` to `/user`. Besides these, there is a landing page to check if the server is running and reachable at its root path at `/` and a fallback route for any unknown paths that return the expected response code. The following sections explore how Listener can be configured to accept specific events, either sent automatically or manually.

## Configuration

Which kind of actions are run can be fully configured from a central configuration file. Future versions of Listener may allow multiple configuration files or passing in a file path as an argument or even reading serialised configuration data straight from standard input. In essence, a configuration file is an Apple property list file (a “plist”) represented in XML. 

### Location

A configuration file is always nested in a `gitlab-listener` directory, currently always holding only one `listener.plist` file, though establishing space for future, finer grained customisation spread across various files.

The initial version looks for the `gitlab-listener` configuration folder in one of the following locations, as full sample paths in sequence:

1. `~/.config/gitlab-listener`
2. `/usr/local/etc/gitlab-listener`
3. `/etc/gitlab-listener`

If no configuration file is found at any of these paths, the process goes into a *demo mode* and subsequently creates a sample configuration at `/usr/local/etc`, reads it and uses it preliminarily. The created sample file can be used as a starting point and serves as a reference that always matches the internal expectations of Listener in cases of uncertainty. It is written through the same means of Encoding/Decoding (by use of the `Codable` protocol) also used when reading custom files.

### Make-Up

As many other serialisation formats, a property list file is essentially a big dictionary with a solid hierarchy of collections, keys and values. The file opens its header, doctype, root tag and a single dictionary tag, as follows:

```xml
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
<dict>
	…
</dict>
```

The configuration file consists of three main sections, (1) one for *server settings* that define how the server runs as a listening process, (2) a second for *GitLab event instructions* and (3) a third for *user instructions*, both of the latter are used to set up actions and reactions.

The server part can be used to specify the HTTP server’s name and port to listen on for any incoming requests. An example for a basic sideline server, as follows:

```xml
<key>Server</key>
<dict>
	<key>name</key>
	<string>localhost</string>
	<key>port</key>
	<integer>8080</integer>
</dict>
```

The instructions part are where the core functionality of Listener is defined and customised. For all supported invokation types, a number of instructions can be specified that are then checked one by one for a match and executed. If more than one instruction matches the criteria of an incoming request, *all* matching instructions are invoked in the order they were found in the process’ configuration.

### Event Actions

Instructions can be set up to react to specific GitLab events. As a structure, each instruction can have some or all of the following properties:

- `command` (a sequence of commands and arguments)
- `invokeForKind` (a GitLab event kind)
- `invokeForRef` (a Git repository reference)

In the following section, we’re going to take a look at an example case: how to react to submitted events by updating a local repository to its most recent version on the currently checked out branch by executing a pull of recently pushed changes into a local checked out copy of a project. This example assumes that a copy of the project to be worked on already exists on the machine running Listener.

```xml
<key>GitLab Instructions</key>
<array>
	<dict>
		<key>command</key>
		<array>
			<string>git pull</string>
		</array>
		<key>invokeForKind</key>
		<string>push</string>
	</dict>
</array>
``` 

The instruction above executes the command `git pull` in Listener’s working directory; in this case `git` is the command and `pull` its only argument. More than one command can be specified in a single instruction. The key `invokeForKind` specifies the kind of GitLab event to react to — in this case, a `push` event.

The deployed project might define its `master` branch as a state that should always be on production. The instruction above can be extended to filter for a specific Git reference — in this case, an identification of the branch, as follows:

```xml
<key>GitLab Instructions</key>
<array>
	<dict>
		<key>command</key>
		<array>
			<string>git pull</string>
		</array>
		<key>invokeForKind</key>
		<string>push</string>
		<key>invokeForRef</key>
		<string>refs/heads/master</string>
	</dict>
</array>
``` 

The existing `command` is reused as-is, as well as the GitLab event kind filter for `invokeForKind`. On top, we specify an `invokeForRef` to only execute our command if the receives *push* event is targeting the repository’s `master` branch.

### User Actions

Besides event-based instructions, Listener can also be configured to react to basic `GET` requests in the form of *user instructions*. A user instruction consists of the fixed endpoint `/user` and an affixed *user tag* that can be used to distinguish between added entries as there is no relation to repository events or references to filter by. A user instruction only consists of a `command` and a `tag`, as follows:

```xml
<key>User Instructions</key>
<array>
	<dict>
		<key>command</key>
		<array>
			<string>echo 'Ping'</string>
		</array>
		<key>tag</key>
		<string>ping</string>
	</dict>
</array>
```

The instruction above reacts to requests sent to `/user/ping` and runs the command `echo 'Ping'` in Listener’s working directory. Just like event instructions, multiple commands can be specified as necessary. The purpose of user actions is to allow for particular commands to be available at any time, from anywhere outside the hosting server. While it’s primary use is debugging, it may also be used to accept requests sent from other servers or for administrative motivations, like fetching recently updated data from a standalone CMS.

### Daemonisation

In common set-ups, a [systemd](https://wiki.debian.org/systemd) service is created to manage Listener as a process that is launched and maintained on start-up and handles restarts should the process crash or the system itself get reset. At the time of writing, service definitions are kept in `/etc/systemd/system` on Linux machines. A definition might be structured as follows:

```
[Unit]
Description=GitLab Listener
After=network.target

[Service]
Type=simple
ExecStart=/usr/local/share/gitlab-listener/GitLabListener
Restart=always

[Install]
WantedBy=multi-user.target
```

This allows Listener to start after network services become available, starts the process once, allowing it to load its configuration from preset locations and restarts it if necessary. With this configuration in place, Listener can now be started and stopped with the following systemd commands:

```sh
systemctl start gitlab-listener      # Start
systemctl stop gitlab-listener       # Stop
```

To find out if Listener is already running or what its state is, as well as the most recent output written to `stdout`:

```sh
systemctl status gitlab-listener     # Status
```

## Feature Outlook

This section explores some future features, changes and extensions under consideration.

### Verification via Secret Token

Note that, until verification of a prior exchanged secret token like GitLab’s `X-Gitlab-Token ` is implemented, any POST request with a valid format sent to the hosting server is accepted. This might be implemented in the *near future* and would deny all non-authorised requests to be discarded. Until then, it is assumed that a normal, small-scale hosted site is not an attack vector for a port scan and submission of fake webhook-mimicking payloads.

### Time-Based Limits

To protect the server against queueing up too many requests, for instance, if an exposed user instruction route is called many times from the outside in short succession, either due to some incorrect configuration, accidental or purposeful misuse, a limit may be imposed to delay or discard additional events.

## Additionals

### Security Considerations

Listener does not indicate its configuration to the outside and the landing page is just to allow a basic availability and reachability check. There is no introspective command to allow an external contacter to check which instructions are supported for which events, the commands to be executed or which user tags were registered to be listened for; of course, a brute force poll through user actions may reveal and also call any configured commands.

Additionally, after a request is received that should be handled, Listener does not send the output produced by any executed commands back to the contacter; anything the called script or process writes to stdout or stderr is discarded after use and can be considered safe from external viewing; the only information the contacter receives back is the HTTP status code of the general procedure, nothing more. If logging or persistence is desired for executions, it is up to the commands themselves to implement and not part of the domain of Listener.

### Alternatives & Comparisons

In common set-ups using GitLab, one or more *Runners* have to be deployed to carry out any build, test or deployment tasks; they are given specific jobs depending on the project's configuration for continuous integration. In contrast to these more evolved set-ups aimed at distributed and scaled systems, Listener is used under the assumption the production server can carry out the necessary tasks on its own and testing has already concluded before changes are pushed to the repository (for instance, locally or by a separate service). Instead of getting new releases that were bundled by a separate service copied over, the hosting server fetches updates on its own and also performs compilation and bundling as necessary.