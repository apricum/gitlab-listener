//
//  Main
//  GitLabListener
//
//  Created by August Saint Freytag on 04/04/2020.
//

import Foundation
import PerfectLib

// Logging

Log.logger = CustomLogger()

// Arguments

let arguments = CommandLine.arguments

if arguments.contains("--help") || arguments.contains("-h") {
	ProcessUtilities.printProcessHelp()
	exit(0)
}

// Launch

ProcessUtilities.prepareProcessConfiguration()
Server.launch()
exit(0)
