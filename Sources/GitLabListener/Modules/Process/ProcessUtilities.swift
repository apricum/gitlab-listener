//
//  ProcessUtilities.swift
//  GitLabListener
//
//  Created by August Saint Freytag on 23/06/2020.
//

import Foundation
import PerfectLib

class ProcessUtilities {

	private init() {}
	
	static func prepareProcessConfiguration() {
		guard let existingConfiguration = Server.configurationFromFile else {
			Log.info(message: "Using example configuration. To use custom actions, create a configuration file in one of the expected paths and restart.")
			Server.commonConfiguration = Server.exampleConfiguration
			Server.writeExampleConfiguration()
			
			return
		}
		
		Server.commonConfiguration = existingConfiguration
	}
	
	static func printProcessHelp() {
		let processName = Self.processName
		let processExampleConfigurationURL = Server.exampleConfigurationDirectoryURL.appendingPathComponent(Server.configurationFileName)
		
		let processHelpText: [String] = [
			"usage: \(processName) [--help]",
			Formatting.sectionBreak,
			"On launch, Listener looks for a configuration file in one of the following locations:",
			Formatting.lineBreak,
			Server.formattedConfigurationURLsListing,
			Formatting.sectionBreak,
			"If no configuration is found, a default configuration is written to '\(processExampleConfigurationURL.relativePath)' and loaded. ",
			"It can be used as a starting point for specifying own instructions. ",
			"The configuration file is an Apple property list file (XML) and allows specifying individual instructions for event-based or user-call-based actions. ",
			"For details on the structure of the configuration file, see the project's documentation."
		]
		
		Log.display(message: processHelpText.joined())
	}
	
	// MARK: Process Text Form
	
	static var defaultProcessName: String { "gitlab-listener" }
	
	static var processName: String { CommandLine.arguments.first?.lastFilePathComponent ?? defaultProcessName }

}

extension ProcessUtilities {

	enum Formatting {
		static var indentation: String { "    " }
		static var lineBreak: String { "\n" }
		static var sectionBreak: String { "\n\n" }
	}

}
