//
//  GitLabUser.swift
//  GitLabListener
//
//  Created by August Saint Freytag on 05/04/2020.
//

import Foundation

struct GitLabUser {
	
	let id: Int
	let name: String
	let userName: String
	let email: String
	let avatar: URLString
	
}

// MARK: Codable

extension GitLabUser: Codable {
	
	private enum CodingKeys: String, CodingKey {
		case id = "user_id"
		case name = "user_name"
		case userName = "user_username"
		case email = "user_email"
		case avatar = "user_avatar"
	}
	
}
