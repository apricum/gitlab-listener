//
//  GitLabEvent.swift
//  GitLabListener
//
//  Created by August Saint Freytag on 05/04/2020.
//

import Foundation

typealias CommitHash = String
typealias RepositoryReference = String
typealias URLString = String

struct GitLabEvent {
	
	let kind: Kind
	let commitHashBefore: CommitHash
	let commitHashAfter: CommitHash
	let checkout: CommitHash
	let ref: RepositoryReference
	let user: GitLabUser
	
}

// MARK: Codable

extension GitLabEvent: Codable {
	
	private enum CodingKeys: String, CodingKey {
		case kind = "object_kind"
		case commitHashBefore = "before"
		case commitHashAfter = "after"
		case checkout = "checkout_sha"
		case ref
		case user
	}
	
	init(from decoder: Decoder) throws {
		let container = try decoder.container(keyedBy: CodingKeys.self)
		
		self.kind = try container.decode(Kind.self, forKey: .kind)
		self.commitHashBefore = try container.decode(CommitHash.self, forKey: .commitHashBefore)
		self.commitHashAfter = try container.decode(CommitHash.self, forKey: .commitHashAfter)
		self.checkout = try container.decode(CommitHash.self, forKey: .checkout)
		self.ref = try container.decode(RepositoryReference.self, forKey: .ref)
		self.user = try GitLabUser(from: decoder)
	}
	
	func encode(to encoder: Encoder) throws {
		var container = encoder.container(keyedBy: CodingKeys.self)
		
		try container.encode(kind, forKey: .kind)
		try container.encode(commitHashBefore, forKey: .commitHashBefore)
		try container.encode(commitHashAfter, forKey: .commitHashAfter)
		try container.encode(checkout, forKey: .checkout)
		try container.encode(ref, forKey: .ref)
		try container.encode(user, forKey: .user)
	}
	
}

// MARK: Library

extension GitLabEvent {
	
	static var noCommitHash: String { String(repeating: "0", count: 40) }
	
	enum Kind: String, Codable {
		case push = "push"
		case tagPush = "tag_push"
		case issue = "issue"
		case note = "note"
		case mergeRequest = "merge_request"
		case wikiPage = "wiki_page"
		case pipeline = "pipeline"
		case build = "build"
	}
	
}
