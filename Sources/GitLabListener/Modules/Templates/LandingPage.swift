//
//  LandingPage.swift
//  GitLabListener
//
//  Created by August Saint Freytag on 04/04/2020.
//

import Foundation

typealias HTMLString = String

func landingPage() -> HTMLString {
	return """
	<!doctype html>
	<html>
	<head>
		<title>GitLab Listener</title>
		<meta charset="utf8" />
		<style>
			body { font-family: -apple-system, sans-serif; font-size: 1.5rem; padding: 75px; }
			div { text-align: center; }
		</style>
	</head>
	<body>
		<div>Listener says &quot;Hello&quot;.</div>
	</body>
	</html>
	"""
}
