//
//  InvokableInstruction.swift
//  GitLabListener
//
//  Created by August Saint Freytag on 11/04/2020.
//

import Foundation

protocol AnyInvokableInstruction {
	
	var command: [String]? { get }
	
}

protocol InvokableInstruction: AnyInvokableInstruction, Codable, Hashable {}

struct UserInstruction: InvokableInstruction {
	
	let command: [String]?
	let tag: String?
	
}

struct GitLabEventInstruction: InvokableInstruction {
	
	let command: [String]?
	let invokeForKind: GitLabEvent.Kind?
	let invokeForRef: RepositoryReference?
	
}
