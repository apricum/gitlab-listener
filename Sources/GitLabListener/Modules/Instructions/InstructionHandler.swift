//
//  InstructionHandler.swift
//  GitLabListener
//
//  Created by August Saint Freytag on 10/04/2020.
//

import Foundation
import PerfectLib

class InstructionHandler {
	
	private init() {}
	
	static func execute(_ instruction: AnyInvokableInstruction) {
		guard let arguments = instruction.command else {
			Log.warning(message: "Executed instruction does not have command arguments. Will be skipped.")
			return
		}
		
		executeShell(arguments)
	}
	
	static func executeShell(_ arguments: [String]) {
		let task = Process()
		let outputPipe = Pipe()
		
		task.launchPath = "/usr/bin/env"
		task.arguments = arguments
		task.standardOutput = outputPipe
		
		task.launch()
		task.waitUntilExit()
		
		let terminationCode = task.terminationStatus
		let outputData = outputPipe.fileHandleForReading.readDataToEndOfFile()
		
		if let outputString = String(data: outputData, encoding: .utf8) {
			Log.info(message: "Completed command with code '\(terminationCode)'.")
			Log.info(message: outputString)
		} else {
			Log.info(message: "Completed command with code '\(terminationCode)', no output.")
		}
	}
	
}
