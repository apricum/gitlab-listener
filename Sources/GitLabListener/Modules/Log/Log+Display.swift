//
//  Log+Display.swift
//  GitLabListener
//
//  Created by August Saint Freytag on 26/09/2020.
//

import Foundation
import PerfectLib

extension Log {
	
	static func display(message: String) {
		StandardStream.printOut(message)
	}
	
}
