//
//  StandardStream.swift
//  GitLabListener
//
//  Created by August Saint Freytag on 26/09/2020.
//

import Foundation

struct StandardStream {
	
	private init() {}
	
	/// Writes the given string into the standard output stream and flushes through immediately.
	@inlinable static func printOut(_ string: String) {
		let lineString = "\(string)\n"
		FileHandle.standardOutput.write(data(from: lineString))
	}
	
	/// Writes the given string into the standard error stream and flushes through immediately.
	@inlinable static func printError(_ string: String) {
		let lineString = "\(string)\n"
		FileHandle.standardError.write(data(from: lineString))
	}
	
	@inlinable static func data(from string: String) -> Data {
		return string.data(using: .utf8)!
	}
	
}
