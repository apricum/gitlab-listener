//
//  CustomLogger.swift
//  GitLabListener
//
//  Created by August Saint Freytag on 26/09/2020.
//

import Foundation
import PerfectLib

struct CustomLogger: Logger {
	
	// MARK: Log
	
	public func debug(message: String, _ even: Bool) {
		StandardStream.printOut("[DBG] \(message)")
	}
	
	public func info(message: String, _ even: Bool) {
		StandardStream.printOut("[INFO] \(message)")
	}
	
	public func warning(message: String, _ even: Bool) {
		StandardStream.printOut("[WARN] \(message)")
	}
	
	public func error(message: String, _ even: Bool) {
		StandardStream.printError("[ERR] \(message)")
	}
	
	public func critical(message: String, _ even: Bool) {
		StandardStream.printError("[CRIT] \(message)")
	}
	
	public func terminal(message: String, _ even: Bool) {
		StandardStream.printError("[TERM] \(message)")
	}
	
}
