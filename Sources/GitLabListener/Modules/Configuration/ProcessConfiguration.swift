//
//  ServerConfiguration.swift
//  GitLabListener
//
//  Created by August Saint Freytag on 08/04/2020.
//

import Foundation

struct ProcessConfiguration: Codable {
	
	enum CodingKeys: String, CodingKey {
		case server = "Server"
		case userInstructions = "User Instructions"
		case gitLabInstructions = "GitLab Instructions"
	}
	
	let server: ProcessServerConfiguration
	let userInstructions: [UserInstruction]?
	let gitLabInstructions: [GitLabEventInstruction]?
	
}

struct ProcessServerConfiguration: Codable {
	
	let name: String
	let port: Int
	
}
