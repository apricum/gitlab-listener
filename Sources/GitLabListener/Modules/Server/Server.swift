//
//  Server.swift
//  GitLabListener
//
//  Created by August Saint Freytag on 08/04/2020.
//

import Foundation
import PerfectLib
import PerfectHTTPServer

class Server: ConfigurableServer, RoutingServer {
	
	static var commonConfiguration: ProcessConfiguration?
	
	// MARK: Init
	
	private init() {}
	
	// MARK: Launch
	
	static func launch() {
		do {
			Log.info(message: "Launching GitLab Listener server.")
			
			let configuration = Server.commonConfiguration
			let name = configuration?.server.name ?? "localhost"
			let port = configuration?.server.port ?? 9000
			let routes = makeRoutes()
			
			try HTTPServer.launch(
				.server(name: name, port: port, routes: routes)
			)
		} catch {
			Log.terminal(message: "Could not launch server. \(error)")
		}
	}
	
}
