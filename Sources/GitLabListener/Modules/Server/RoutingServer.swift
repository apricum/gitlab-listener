//
//  Server.swift
//  GitLabListener
//
//  Created by August Saint Freytag on 04/04/2020.
//

import Foundation
import PerfectHTTP
import PerfectLib

protocol RoutingServer {}

extension RoutingServer {

	// MARK: Routes
	
	static func makeRoutes() -> Routes {
		var routes = Routes()
		
		routes.add(method: .get, uri: "/") { request, response in
			response.setHeader(.contentType, value: "text/html")
			response.appendBody(string: landingPage())
			response.completed()
		}
		
		routes.add(method: .post, uri: "/event") { request, response in
			Log.info(message: "Received event at hook endpoint.")
			
			do {
				guard let requestData = request.postBodyString?.data(using: .utf8) else {
					Log.error(message: "Could not form unicode data from received request body string.")
					return
				}
				
				let decoder = JSONDecoder()
				let event = try decoder.decode(GitLabEvent.self, from: requestData)
				Log.info(message: "Successfully received and decoded event \(event).")
				
				executeMatchingInstructions(for: event)
				response.completed(status: .ok)
			} catch {
				Log.error(message: "Could not form usable event from request. \(error.localizedDescription)")
				response.completed(status: .badRequest)
			}
		}
		
		routes.add(method: .get, uri: "/user/{tag}") { request, response in
			let userEventTag = request.urlVariables["tag"]
			let event = UserEvent(tag: userEventTag)
			
			Log.info(message: "Received event at user endpoint with tag '\(userEventTag ?? "<None>")'.")
			
			executeMatchingInstructions(for: event)
			response.completed(status: .ok)
		}
		
		routes.add(method: .get, uri: "*") { request, response in
			response.completed(status: .notFound)
		}
		
		return routes
	}
	
	// MARK: Instructions
	
	static func executeMatchingInstructions(for event: UserEvent) {
		Server.commonConfiguration?.userInstructions?.forEach { instruction in
			if let matchingTag = instruction.tag, event.tag != matchingTag {
				return
			}
			
			InstructionHandler.execute(instruction)
		}
	}
	
	static func executeMatchingInstructions(for event: GitLabEvent) {
		Server.commonConfiguration?.gitLabInstructions?.forEach { instruction in
			if let matchingKind = instruction.invokeForKind, event.kind != matchingKind {
				return
			}
			
			if let matchingRef = instruction.invokeForRef, event.ref != matchingRef {
				return
			}
			
			InstructionHandler.execute(instruction)
		}
	}

}
