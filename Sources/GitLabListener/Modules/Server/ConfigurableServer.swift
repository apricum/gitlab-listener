//
//  ServerConfiguration.swift
//  GitLabListener
//
//  Created by August Saint Freytag on 08/04/2020.
//

import Foundation
import PerfectLib

protocol ConfigurableServer {
	
	static var commonConfiguration: ProcessConfiguration? { get set }
	
}

// MARK: Defaults

extension ConfigurableServer {
	
	// MARK: Path Components
	
	static var configurationDirectoryURLs: [URL] {[
		URL(fileURLWithPath: "~/.config", isDirectory: true),
		URL(fileURLWithPath: "/usr/local/etc", isDirectory: true),
		URL(fileURLWithPath: "/etc", isDirectory: true)
	]}
	
	static var exampleConfigurationDirectoryURL: URL {
		URL(fileURLWithPath: "/usr/local/etc", isDirectory: true)
	}
	
	static var configurationDirectoryName: String { "gitlab-listener" }
	static var configurationFileName: String { "listener.plist" }
	
	// MARK: Path Composites
	
	static var configurationFileURLs: [URL] {
		configurationDirectoryURLs.map { baseURL in
			baseURL.appendingPathComponent(configurationDirectoryName).appendingPathComponent(configurationFileName)
		}
	}
	
	static var exampleConfigurationFileURL: URL {
		exampleConfigurationDirectoryURL.appendingPathComponent(configurationDirectoryName).appendingPathComponent(configurationFileName)
	}
	
}

// MARK: Implementation

extension ConfigurableServer {
	
	static var environment: [String: String] { ProcessInfo.processInfo.environment }
	static var workingDirectoryURL: URL { URL(fileURLWithPath: CommandLine.arguments[0]).deletingLastPathComponent() }
	
	// MARK: Read
	
	static func firstUsableConfigurationFile() -> (url: URL, handle: FileHandle)? {
		for configurationFileURL in configurationFileURLs {
			guard let fileHandle = try? FileHandle(forReadingFrom: configurationFileURL) else {
				continue
			}
			
			return (configurationFileURL, fileHandle)
		}
		
		return nil
	}
	
	static var configurationFromFile: ProcessConfiguration? {
		do {
			guard let (configurationFileURL, configurationFile) = firstUsableConfigurationFile() else {
				Log.warning(message: "Could not find a configuration file in any available default path. Expected a file at one of the following paths: \n\(formattedConfigurationURLsListing)")
				return nil
			}
			
			let configurationData = configurationFile.readDataToEndOfFile()
			configurationFile.closeFile()
			
			let decoder = PropertyListDecoder()
			let configuration = try decoder.decode(ProcessConfiguration.self, from: configurationData)
			
			Log.info(message: "Read configuration file from '\(configurationFileURL.relativePath)'.")
			return configuration
		} catch {
			Log.error(message: "Could not read configuration file. \(error.localizedDescription)")
			return nil
		}
	}
	
	static var formattedConfigurationURLsListing: String {
		let lines = configurationFileURLs.enumerated().reduce(into: [String]()) { lines, pair in
			let (index, fileURL) = pair
			let description = "\(ProcessUtilities.Formatting.indentation)(\(index + 1)) \(fileURL.relativePath)"
			
			lines.append(description)
		}
		
		return lines.joined(separator: "\n")
	}
	
	// MARK: Write
	
	static func writeExampleConfiguration() {
		let configurationFileURL = exampleConfigurationFileURL
		
		do {
			try FileManager.default.createDirectory(
				at: configurationFileURL.deletingLastPathComponent(),
				withIntermediateDirectories: true,
				attributes: nil
			)
			
			let configurationData = exampleConfigurationData
			try configurationData.write(to: configurationFileURL)
		} catch {
			Log.error(message: "Could not write example configuration file to path '\(configurationFileURL.relativePath)'. \(error.localizedDescription)")
		}
	}
	
	static var exampleConfigurationData: Data {
		let encoder = PropertyListEncoder()
		encoder.outputFormat = .xml
		
		let configuration = exampleConfiguration
		let configurationData = try! encoder.encode(configuration)
		
		return configurationData
	}
	
	static var exampleConfiguration: ProcessConfiguration {
		ProcessConfiguration(
			server: ProcessServerConfiguration(name: "localhost", port: 9000),
			userInstructions: [
				UserInstruction(command: ["echo", "'Hello, listeners.'"], tag: "greet")
			],
			gitLabInstructions: [
				GitLabEventInstruction(command: ["echo", "'Hello from GitLab.'"], invokeForKind: .push, invokeForRef: "refs/heads/master")
			]
		)
	}
	
}

struct ServerConfigurationError: LocalizedError {
	
	var errorDescription: String?
	
}
